import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task-dto';
import { User, Task } from '@prisma/client';
import { Header } from '@nestjs/common';

@Controller('tasks')
export class TasksController {
  constructor(private taskService: TasksService) {}

  @Get()
  getTasks(): Promise<Task[]> {
    return this.taskService.getTasks();
  }

  @Get('/:id')
  getTaskById(@Param('id') id: number): Promise<Task> {
    return this.taskService.getTaskById(+id);
  }

  @Post()
  createTask(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
    return this.taskService.createTask(createTaskDto);
  }

  @Delete('/:id')
  removeTask(@Param('id') id: number): Promise<void> {
    return this.taskService.removeTask(+id);
  }

  @Patch('/:id')
  checkTask(@Param('id') id: number): Promise<void> {
    return this.taskService.checkTask(+id);
  }

  // @Patch('/:id/check')
  // checkTask(@Param('id') id: string): void {
  //   return this.taskService.checkTask(id);
  // }
}
