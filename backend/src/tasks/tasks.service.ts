import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task-dto';
import { PrismaService } from 'src/prisma.service';
import { User, Task } from '@prisma/client';
import { NotFoundException } from '@nestjs/common/exceptions';

@Injectable()
export class TasksService {
  constructor(private prisma: PrismaService) {}

  async getTasks(): Promise<Task[]> {
    const tasks = await this.prisma.task.findMany({
      orderBy: {
        datePosted: 'desc',
      },
    });
    return tasks;
  }

  async getTaskById(id: number): Promise<Task> {
    const task = await this.prisma.task.findUnique({
      where: {
        id: id,
      },
    });

    if (!task) {
      throw new NotFoundException('Заметки с таким ID не существует');
    }
    return task;
  }

  async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    const { title, content } = createTaskDto;
    const task = await this.prisma.task.create({
      data: {
        user: {
          connect: {
            id: 1,
          },
        },
        title,
        content,
      },
    });
    return task;
  }

  async removeTask(id: number): Promise<void> {
    try {
      await this.prisma.task.delete({
        where: {
          id: id,
        },
      });
    } catch (error) {
      throw new NotFoundException('Заметки с таким ID не существует');
    }
  }

  async checkTask(id: number): Promise<void> {
    const task = await this.getTaskById(id);
    await this.prisma.task.update({
      where: {
        id: id,
      },
      data: {
        checked: task.checked ? false : true,
      },
    });
  }
}
