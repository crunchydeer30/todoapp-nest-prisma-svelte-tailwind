/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'primary': '#50e3a4',
        'primary-darken': '#35de96',
        'secondary': '#f0efe9',
        'tertiart': '#d4d4d4',
        'primary-font': '#777',
        'secondary-font': '#b6b6b6',
      }
    },
  },
  plugins: [],
}

